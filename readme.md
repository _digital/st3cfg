### Sublime Text 3 Configuration Repo

This repository contains packages downloaded from [package control](https://packagecontrol.io) and configuration files for my personal ST3 setup.

#### How to

If you would like to use my setup, first install Package Control then clone this repo to your `Packages\User` folder.
